package story

import (
	"encoding/json"
	"io"
	"io/ioutil"
)

// Story represents a cyoa story with multiple pages
// and story arcs
type Story struct {
	Arcs map[string]Arc
}

// Arc represents a page of the story with options
// for navigation
type Arc struct {
	Title   string
	Story   []string
	Options []Option
}

// Option represents a path to another StoryArc
type Option struct {
	Text string
	Arc  string
}

// NewStory creates a new Story from the supplied reader
func NewStory(r io.Reader) (*Story, error) {
	var story *Story

	jsonBytes, err := ioutil.ReadAll(r)

	if err == nil {
		err := json.Unmarshal(jsonBytes, story)
		if err == nil {
			return story, nil
		} else {
			return nil, err
		}
	}

	return nil, err
}
